/* 公共js方法 */
define([
    'toastr',
], function(toastr) {
    'use strict';

    return {
        debug: true,
        ip: 'http://localhost:5757/weapp/',
        ajax: function(url, param, async, type, timeout) {
            var self = this;
            return $.ajax({
                url: self.ip + url,
                data: param || {},
                type: type || 'POST',
                async: async,
                timeout: timeout || 1000000000,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("user_uid", '');
                },
            });
        },
        getUrl: function(action) {
            return this.ip + action;
        },
        handleAjax: function(url, param, load, autoToastr, async, type, timeout) {
            var self = this;
            if (load) var loadId = this.showLoading();
            return this.ajax(url, param, async, type).then(function(resp) {
                if (self.debug && console) console.log(resp);
                if (load) self.closeLoading(loadId);
                return resp;
            }, function(err) {
                if (autoToastr) {
                    if (err.statusText == "timeout") {
                        toastr.error('网络超时，请求失败。');
                    } else {
                        toastr.error('系统错误，请联系管理员。');
                    }
                }
                if (load) self.closeLoading(loadId);
                return err.statusText;
            });
        },
        showLoading: function() {
            return layer.load(2, {
                shade: [0.5, '#fff'] // 0.5透明度的白色背景
            });
        },
        closeLoading: function(i) {
            layer.close(i);
        }
    }
});

Date.prototype.Format = function(fmt) { // author: meizz 
    var o = {
        "M+": this.getMonth() + 1, // 月份 
        "d+": this.getDate(), // 日 
        "h+": this.getHours(), // 小时 
        "m+": this.getMinutes(), // 分 
        "s+": this.getSeconds(), // 秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), // 季度 
        "S": this.getMilliseconds() // 毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}