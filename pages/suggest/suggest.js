define(['bootstrapSuggest'], function() {
    return {
        init: function() {
            var testBsSuggest = $("#suggest #test").bsSuggest({
                url: "./pages/suggest/data.json",
                idField: "userId",
                keyField: "userName"
            }).on("onDataRequestSuccess", function(e, result) {
                // console.log("onDataRequestSuccess: ", result)
            }).on("onSetSelectValue", function(e, keyword) {
                // console.log("onSetSelectValue: ", keyword)
            }).on("onUnsetSelectValue", function(e) {
                // console.log("onUnsetSelectValue")
            });
            var testBsSuggest = $("#testNoBtn").bsSuggest({
                url: "./pages/suggest/data.json",
                showBtn: false,
                idField: "userId",
                keyField: "userName"
            }).on("onDataRequestSuccess", function(e, result) {
                // console.log("onDataRequestSuccess: ", result)
            }).on("onSetSelectValue", function(e, keyword) {
                // console.log("onSetSelectValue: ", keyword)
            }).on("onUnsetSelectValue", function(e) {
                // console.log("onUnsetSelectValue")
            });
            var testdataBsSuggest = $("#test_data").bsSuggest({
                indexId: 2,
                indexKey: 1,
                data: {
                    "value": [{
                        "id": "0",
                        "word": "lzw",
                        "description": "http://lzw.me"
                    }, {
                        "id": "1",
                        "word": "lzwme",
                        "description": "http://w.lzw.me"
                    }, {
                        "id": "2",
                        "word": "meizu",
                        "description": "http://www.meizu.com"
                    }, {
                        "id": "3",
                        "word": "flyme",
                        "description": "http://flyme.meizu.com"
                    }],
                    "defaults": "http://lzw.me"
                }
            });
            var baiduBsSuggest = $("#baidu").bsSuggest({
                allowNoKeyword: false,
                multiWord: true,
                separator: ",",
                getDataMethod: "url",
                url: "http://unionsug.baidu.com/su?p=3&t=" + (new Date()).getTime() + "&wd=",
                jsonp: "cb",
                processData: function(json) {
                    var i, len, data = {
                        value: []
                    };
                    if (!json || !json.s || json.s.length === 0) {
                        return false
                    }
                    // console.log(json);
                    len = json.s.length;
                    jsonStr = "{'value':[";
                    for (i = 0; i < len; i++) {
                        data.value.push({
                            word: json.s[i]
                        })
                    }
                    data.defaults = "baidu";
                    return data
                }
            });
            var taobaoBsSuggest = $("#taobao").bsSuggest({
                indexId: 2,
                indexKey: 1,
                allowNoKeyword: false,
                multiWord: true,
                separator: ",",
                getDataMethod: "url",
                effectiveFieldsAlias: {
                    Id: "序号",
                    Keyword: "关键字",
                    Count: "数量"
                },
                showHeader: true,
                url: "http://suggest.taobao.com/sug?code=utf-8&extras=1&q=",
                jsonp: "callback",
                processData: function(json) {
                    var i, len, data = {
                        value: []
                    };
                    if (!json || !json.result || json.result.length == 0) {
                        return false
                    }
                    // console.log(json);
                    len = json.result.length;
                    for (i = 0; i < len; i++) {
                        data.value.push({
                            "Id": (i + 1),
                            "Keyword": json.result[i][0],
                            "Count": json.result[i][1]
                        })
                    }
                    // console.log(data);
                    return data
                }
            });
            $("form").submit(function(e) {
                return false
            });
        },
        destroy: function() {// 内存释放
        }
    }
});
