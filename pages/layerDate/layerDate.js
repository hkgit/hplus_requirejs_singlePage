define(['laydate', 'common'], function(laydate, common) {
    return {
        init: function() {
            laydate.path = './node_modules/layui-laydate/src/'; // laydate.js 所在目录
            var today_day = new Date().getDate();
            var firstDate_day = new Date(new Date().setDate(1)).getDate();
            var endDate_day = laydate.getEndDate(new Date().getMonth() + 1);
            laydate.render({
                // position: 'static',
                elem: '#layerDate #test',
            });
            laydate.render({
                // position: 'static',
                elem: '#layerDate #test1',
                min: firstDate_day - today_day,
                max: endDate_day - today_day
            });
            laydate.render({
                // position: 'static',
                elem: '#layerDate #test2',
                min: firstDate_day - today_day,
                max: endDate_day - today_day,
                range: true
            });
            var test3 = laydate.render({
                elem: '#layerDate #test3',
                min: new Date().Format('yyyy-MM-dd'),
                done: function(value, date) {
                    console.log("test3 done");
                    $.map(test4.config.min, function(value, key) {
                        test4.config.min[key] = key === 'month' ? date[key] - 1 : date[key];
                    });
                    $.map(test4.config.max, function(value, key) {
                        test4.config.max[key] = key === 'month' ? date[key] - 1 : key === 'date' ? laydate.getEndDate(date.month) : date[key];
                    });
                    if (value) {
                        $('#test4').removeAttr('disabled');
                        if (value != $('#test3').val()) {
                            $('#test4').val('');
                        }
                    } else {
                        $('#test4').remove('disabled', true).val('');
                    }
                }
            });
            var test4 = laydate.render({
                elem: '#layerDate #test4',
                done: function(value, date) {
                    console.log("test4 done");
                    // $.map(test3.config.min, function(value, key) {
                    //     test3.config.min[key] = key === 'month' ? date[key] - 1 : key === 'date' ? 1 : date[key];
                    // });
                    // $.map(test3.config.max, function(value, key) {
                    //     test3.config.max[key] = key === 'month' ? date[key] - 1 : date[key];
                    // });
                }
            });
        },
        destroy: function() { // 内存释放
        }
    }
});